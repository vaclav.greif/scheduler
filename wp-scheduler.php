<?php

/*
 * Plugin Name: WP Scheduler
 * Version: 0.1.0
 * Text Domain:     wp-scheduler
 * Domain Path:     /languages
*/

use ComposePress\Dice\Dice;


/**
 * Singleton instance function. We will not use a global at all as that defeats the purpose of a singleton and is a bad design overall
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @return Wp_Scheduler\Plugin
 */
function wp_scheduler() {
	return wp_scheduler_container()->create( '\Wp_Scheduler\Plugin' );
}

/**
* This container singleton enables you to setup unit testing by passing an environment file to map classes in Dice
 *
 * @param string $env
 *
* @return \ComposePress\Dice\Dice
 */
function wp_scheduler_container( $env = 'prod' ) {
	static $container;
	if ( empty( $container ) ) {
		$container = new Dice();
		include __DIR__ . "/config_{$env}.php";
	}

	return $container;
}

/**
 * Init function shortcut
 */
function wp_scheduler_init() {
	wp_scheduler()->init();
}

/**
 * Activate function shortcut
 */
function wp_scheduler_activate( $network_wide ) {
	register_uninstall_hook( __FILE__, 'wp_scheduler_uninstall' );
	wp_scheduler()->init();
	wp_scheduler()->activate( $network_wide );
}

/**
 * Deactivate function shortcut
 */
function wp_scheduler_deactivate( $network_wide ) {
	wp_scheduler()->deactivate( $network_wide );
}

/**
* Uninstall function shortcut
*/
function wp_scheduler_uninstall() {
	wp_scheduler()->uninstall();
}

/**
 * Error for older php
 */
function wp_scheduler_php_upgrade_notice() {
	$info = get_plugin_data( __FILE__ );
	_e(
		sprintf(
			'
	<div class="error notice">
		<p>Opps! %s requires a minimum PHP version of 5.4.0. Your current version is: %s. Please contact your host to upgrade.</p>
	</div>', $info['Name'], PHP_VERSION
		)
	);
}

/**
 * Error if vendors autoload is missing
 */
function wp_scheduler_php_vendor_missing() {
	$info = get_plugin_data( __FILE__ );
	_e(
		sprintf(
			'
	<div class="error notice">
		<p>Opps! %s is corrupted it seems, please re-install the plugin.</p>
	</div>', $info['Name']
		)
	);
}

/*
 * We want to use a fairly modern php version, feel free to increase the minimum requirement
 */
if ( version_compare( PHP_VERSION, '5.4.0' ) < 0 ) {
	add_action( 'admin_notices', 'wp_scheduler_php_upgrade_notice' );
} else {
	if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
		include_once __DIR__ . '/vendor/autoload.php';
		add_action( 'plugins_loaded', 'wp_scheduler_init', 11 );
		register_activation_hook( __FILE__, 'wp_scheduler_activate' );
		register_deactivation_hook( __FILE__, 'wp_scheduler_deactivate' );
	} else {
		add_action( 'admin_notices', 'wp_scheduler_php_vendor_missing' );
	}
}

require_once 'src/pluggables.php';