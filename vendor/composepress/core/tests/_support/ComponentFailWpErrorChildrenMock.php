<?php


use ComposePress\Core\Abstracts\Component_0_9_0_0;

class ComponentFailWpErrorChildrenMock extends Component_0_9_0_0 {

	private $child;
	private $child2;

	public function __construct() {
		$this->child  = new ComponentChildFailWpErrorMock();
		$this->child2 = new ComponentChildFailWpErrorMock();
	}

	/**
	 * @return \ComponentChildMock
	 */
	public function get_child() {
		return $this->child;
	}
}
