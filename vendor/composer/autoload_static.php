<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5b4af6e8a6bc28f489a137eca7560cf8
{
    public static $prefixLengthsPsr4 = array (
        'W' => 
        array (
            'Wp_Scheduler\\' => 13,
        ),
        'C' => 
        array (
            'ComposePress\\Dice\\' => 18,
            'ComposePress\\Core\\' => 18,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Wp_Scheduler\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
        'ComposePress\\Dice\\' => 
        array (
            0 => __DIR__ . '/..' . '/composepress/dice',
        ),
        'ComposePress\\Core\\' => 
        array (
            0 => __DIR__ . '/..' . '/composepress/core/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5b4af6e8a6bc28f489a137eca7560cf8::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5b4af6e8a6bc28f489a137eca7560cf8::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
